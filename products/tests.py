from rest_framework.test import (
    APITestCase,
)
from rest_framework import (
    status,
)
from rest_framework.reverse import (
    reverse,
)
from rest_framework_simplejwt.tokens import (
    RefreshToken,
)
from django.contrib.auth import (
    get_user_model,
)
from products.models import (
    Category,
    Product,
)


User = get_user_model()

class ProductListViewTest(APITestCase):
    def setUp(self):
        self.user = User.objects.create_user(
            username='testuser',
            password='testpassword',
            email='test@user.com'
        )
        self.access_token = str(RefreshToken.for_user(self.user).access_token)

        self.category = Category.objects.create(name='Category')
        self.product1 = Product.objects.create(
            name='Product 1',
            description='Description 1',
            price=10.0,
            category=self.category,
        )
        self.product2 = Product.objects.create(
            name='Product 2',
            description='Description 2',
            price=20.0,
            category=self.category,
        )

    def test_product_list_authenticated(self):
        url = reverse('products:product-list')
        response = self.client.get(url, HTTP_AUTHORIZATION=f'Bearer {self.access_token}')

        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(len(response.data), 2)
        self.assertEqual(response.data[0]['name'], 'Product 1')
        self.assertEqual(response.data[1]['name'], 'Product 2')

    def test_product_list_unauthenticated(self):
        url = reverse('products:product-list')
        response = self.client.get(url)

        self.assertEqual(response.status_code, status.HTTP_401_UNAUTHORIZED)

class ExportProductsExcelTestCase(APITestCase):
    def setUp(self):
        self.user = User.objects.create_user(
            username='testuser',
            password='testpassword',
            email='test@user.com'
        )
        self.access_token = str(RefreshToken.for_user(self.user).access_token)

        self.category = Category.objects.create(name='Category')
        self.product1 = Product.objects.create(
            name='Product 1',
            description='Description 1',
            price=10.0,
            category=self.category,
        )
        self.product2 = Product.objects.create(
            name='Product 2',
            description='Description 2',
            price=20.0,
            category=self.category,
        )

    def test_export_products_unauthenticated(self):
        url = reverse('products:export-products')
        response = self.client.get(url)

        self.assertEqual(response.status_code, status.HTTP_401_UNAUTHORIZED)

    def test_export_products_authenticated(self):
        url = reverse('products:export-products')
        response = self.client.get(url, HTTP_AUTHORIZATION=f'Bearer {self.access_token}')

        self.assertEqual(response.status_code, status.HTTP_200_OK)
