from rest_framework import (
    generics,
)
from rest_framework.permissions import (
    IsAuthenticated,
)
from .models import (
    Product,
)
from .serializers import (
    ProductSerializer,
)
from django.utils.decorators import (
    method_decorator,
)
from django.views.decorators.cache import (
    cache_page,
)
from rest_framework.decorators import (
    api_view,
    permission_classes,
)
from django.http import (
    HttpResponse,
)
from io import (
    BytesIO,
)
import pandas as pd
from rest_framework.decorators import (
    api_view,
    permission_classes,
)
from rest_framework.permissions import (
    IsAuthenticated,
)
from rest_framework.pagination import (
    PageNumberPagination,
)


class ProductListView(generics.ListAPIView):
    queryset = Product.objects.select_related('category').prefetch_related('tags')
    serializer_class = ProductSerializer
    permission_classes = [IsAuthenticated]
    pagination_class = PageNumberPagination

    @method_decorator(cache_page(60 * 60))
    def get(self, request, *args, **kwargs):
        return super().get(request, *args, **kwargs)

@api_view(['GET'])
@permission_classes([IsAuthenticated])
def export_products_to_excel(request):
    output = BytesIO()

    products = Product.objects.all()
    product_data = [{'Name': product.name, 'Description': product.description, 'Category': product.category.name, 'Price': product.price, 'Tag': product.tags.all(), 'Created at': product.created_at.replace(tzinfo=None)} for product in products]
    df = pd.DataFrame(product_data)

    writer = pd.ExcelWriter(output, engine='xlsxwriter')
    df.to_excel(writer, sheet_name='Products', index=False)

    writer.close()

    response = HttpResponse(content_type='application/vnd.openxmlformats-officedocument.spreadsheetml.sheet')
    response['Content-Disposition'] = 'attachment; filename=products.xlsx'
    output.seek(0)
    response.write(output.read())

    with open('exported_files/products.xlsx', 'wb') as file:
        file.write(output.getvalue())

    return response
