from rest_framework import (
    serializers,
)
from .models import (
    Product,
)

class ProductSerializer(serializers.ModelSerializer):
    category_name = serializers.CharField(source='category.name')
    tags = serializers.SlugRelatedField(many=True, read_only=True, slug_field='name')

    class Meta:
        model = Product
        fields = ['id', 'name', 'description', 'category_name', 'price', 'created_at', 'tags']
