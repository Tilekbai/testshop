from django.urls import (
    path,
)
from products.views import (
    ProductListView,
    export_products_to_excel,
)


app_name = 'products'

urlpatterns = [
    path('api/products/', ProductListView.as_view(), name='product-list'),
    path('api/products/export/', export_products_to_excel, name='export-products'),
]
