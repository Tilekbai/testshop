from django.urls import (
    reverse,
)
from django.test import (
    TestCase,
)
from django.contrib.auth import (
    get_user_model,
)
from rest_framework.test import (
    APIClient,
    APITestCase,
)


User = get_user_model()

class UserRegistrationTestCase(APITestCase):
    def setUp(self):
        self.client = APIClient()
        self.register_url = reverse('users:user-registration')

    def test_valid_registration(self):
        data = {
            'username': 'newuser',
            'email': 'newuser@example.com',
            'password': 'securepassword'
        }
        response = self.client.post(self.register_url, data, format='json')
        self.assertEqual(response.status_code, 201)
        self.assertEqual(response.data['message'], 'User registered successfully.')

    def test_invalid_registration_missing_fields(self):
        data = {
            'username': 'newuser',
        }
        response = self.client.post(self.register_url, data, format='json')
        self.assertEqual(response.status_code, 400)

class AuthenticationTest(TestCase):
    def setUp(self):
        self.client = APIClient()
        self.user = User.objects.create_user(username='testuser', password='testpassword', email='test@user.com')

    def test_valid_user_authentication(self):
        response = self.client.post('/api/login/', {
            'username': 'testuser',
            'password': 'testpassword'
        }, format='json')
        self.assertEqual(response.status_code, 200)
        self.assertTrue('access' in response.data)
        self.assertTrue('refresh' in response.data)

    def test_invalid_user_authentication(self):
        response = self.client.post('/api/login/', {
            'username': 'testuser',
            'password': 'wrongpassword'
        }, format='json')
        self.assertEqual(response.status_code, 401)
