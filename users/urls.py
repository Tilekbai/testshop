from django.urls import (
    path,
)
from .views import (
    UserRegistrationView,
    UserAuthenticationView,
)


app_name = 'users'

urlpatterns = [
    path('api/register/', UserRegistrationView.as_view(), name='user-registration'),
    path('api/login/', UserAuthenticationView.as_view(), name='user-login'),
]
