from rest_framework.views import (
    APIView,
)
from rest_framework.response import (
    Response,
)
from rest_framework import (
    status,
)
from rest_framework_simplejwt.serializers import (
    TokenObtainPairSerializer,
)
from rest_framework_simplejwt.views import (
    TokenObtainPairView,
)
from .serializers import (
    UserRegistrationSerializer,
    UserAuthenticationSerializer,
)


class UserRegistrationView(APIView):
    def post(self, request):
        serializer = UserRegistrationSerializer(data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response({"message": "User registered successfully."}, status=status.HTTP_201_CREATED)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

class UserAuthenticationView(TokenObtainPairView):
    serializer_class = TokenObtainPairSerializer
